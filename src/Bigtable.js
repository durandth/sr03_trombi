import React from 'react';
import Codemail from "./Codemail";
import AvatarImage from "./Avatar";
import Codetel from './Codetel';
import './App.css';

import Button from 'react-bootstrap/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';


const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },

});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: '#7da7ad',
    color: theme.palette.common.white,
    fontSize: 20,
    fontWeight: "bold",
  },
  body: {
    fontSize: 13,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    "&:hover": {
      backgroundColor: "#bdc3c7"
    }
  },
}))(TableRow);

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

class Bigtable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      page: 0,
      rowsPerPage: 10,
      nom_dialog: '',
      mail_dialog: '',
      photo_dialog: '',
      tel_dialog: '',
      fonc_dialog: '',
      previous_query: '',
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleChangePage = this.handleChangePage.bind(this);
    this.getPrenom = this.getPrenom.bind(this);
  }

  handleClickOpen(item, e) {
    this.setState({
      open: true,
      nom_dialog: item.nomp,
      mail_dialog: item.mail,
      photo_dialog: item.photo,
      tel_dialog: item.telPoste1,
      fonc_dialog: item.fonction
    })
  };

  handleClose(e) {
    this.setState({ open: false })
  };

  handleChangePage(e, newPage) {
    this.setState({ page: newPage })
  };

  getNom(nomPrenom){
    let nomPrenomTab = nomPrenom.split(' ');
    let nomTab = [];
    let i = 0
    while(isUpperCase(nomPrenomTab[i])){
      nomTab.push(nomPrenomTab[i]);
      i++;
    }
    return nomTab.join(' ');
  }

  getPrenom(nomPrenom){
    return nomPrenom.slice(this.getNom(nomPrenom).length + 1);
  }

  componentDidUpdate() {
    const query = this.props.nom.concat(this.props.prenom);
    if (query !== this.state.previous_query){
      this.setState({ previous_query: query});
      this.setState({ page: 0 });
    }
  }

  render() {
    const { err, isLoaded, nom, prenom, lab } = this.props;
    var list = this.props.uvs.filter(function (item) {
      var flag1 = item.nomAz && item.nomAz.indexOf(nom) !== -1 && item.prenomAz && item.prenomAz.indexOf(prenom) !== -1;
      if (lab === "TOUS") {
        return flag1;
      }
      else if (!lab) {
        return flag1 && !item.structLibelleFils;
      }
      else {
        return item.structLibelleFils && flag1 && item.structLibelleFils.indexOf(lab) !== -1;
      }

    })
    if (err) {
      return <div>Erreur : {err.message}</div>;
    } else if (!isLoaded) {
      return <div>Chargement…</div>;
    } else {
      return (
        <div>
          <TableContainer>
            <Table border={1} cellSpacing={"2"}>
              <TableHead >
                <TableRow style={{ backgroundColor: "lightblue" }}>
                  <StyledTableCell> Nom</StyledTableCell>
                  <StyledTableCell> Prénom</StyledTableCell>
                  {/*<th> Email</th>*/}
                  <StyledTableCell> Structure</StyledTableCell>
                  <StyledTableCell> ArbreFils</StyledTableCell>
                  <StyledTableCell> Laboratoire</StyledTableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {list.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map(item => (
                  <StyledTableRow key={item.login} onClick={this.handleClickOpen.bind(this, item)} >
                    <StyledTableCell>{this.getNom(item.nomp)}</StyledTableCell>
                    <StyledTableCell style={{textTransform: 'capitalize'}}>{this.getPrenom(item.nomp)}</StyledTableCell>
                    {/*<TableCell>{item.mail}</TableCell>*/}
                    <StyledTableCell style={{textTransform: 'capitalize'}}>{item.structureAbr}</StyledTableCell>
                    <StyledTableCell style={{textTransform: 'capitalize'}}>{item.structAbrFils}</StyledTableCell>
                    <StyledTableCell style={{textTransform: 'capitalize'}}>{item.structLibelleFils}</StyledTableCell>
                  </StyledTableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10]}
            component="div"
            count={list.length}
            page={list.length && list.length <= 10 && this.state.page !== 0 ? 0 : this.state.page}
            onPageChange={this.handleChangePage}
            rowsPerPage={this.state.rowsPerPage}
          />

          <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open}>
            <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>{this.state.nom_dialog}</DialogTitle>
            <Divider light />
            <Grid container>
              <Grid item xs={4}>
                <AvatarImage photo={this.state.photo_dialog} />
              </Grid>
              <Grid item xs={4}>
                QR-mail:<br/>
                <Codemail mail={this.state.mail_dialog} /><br/>
                Mail : {this.state.mail_dialog}
              </Grid>
              <Grid item xs={4}>
                QR-tel:<br/>
                <Codetel tel={this.state.tel_dialog} /><br/>
                Tel : {this.state.tel_dialog}
              </Grid>
              <Grid item xs={9} style={{padding: '20px'}}>
                Fonction : <br/>
                {this.state.fonc_dialog}
              </Grid>
            </Grid>
            <DialogActions>
              <Button autoFocus onClick={this.handleClose} color="primary">Close</Button>
            </DialogActions>
          </Dialog>
        </div>

      );
    }


  }
}
export default Bigtable;

function isUpperCase(str) {
  return str === str.toUpperCase();
}
