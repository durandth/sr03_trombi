import React from 'react';
import QRCode from 'qrcode.react'
import './App.css';


class Codemail extends React.Component {
  mail = this.props.mail;
  render() {
    return (
      <QRCode
        value={this.mail}
        size={100}
        fgColor="#000000"
      />
    );
  }
}

export default Codemail;
